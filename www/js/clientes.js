$(function() {
  var endPoint = window.localStorage.getItem('end_point');
  var user = JSON.parse(window.localStorage.getItem('user'));

  $.get(endPoint + 'customers/my_customers?cobrador_id=' + user.debt_collector_id, function(data) {
    var ulClientes = $('#lista-clientes');
    window.localStorage.setItem('clientes', JSON.stringify(data));
    $.each(data, function(index, customer) {
      var template = $('#template-li-clientes').html();
      template = template.replace('{{nombre}}', customer.nombre_completo)
                         .replace('{{telefono}}', customer.telefono)
                         .replace('{{direccion}}', customer.direccion)
                         .replace('{{deuda}}', customer.adeudado)
                         .replace('{{id_cliente}}', customer.id);
      ulClientes.append(template);
    });
  });

  window.mostrarCliente = function (idCliente) {
    var clientes = JSON.parse(window.localStorage.getItem('clientes'));
    var cliente;
    $.each(clientes, function(index, c) {
      if (c.id == idCliente) {
        cliente = c;
        return false;
      }
    });

    $('#db_dni_cliente').html(cliente.dni);
    $('#db_nro_cliente').html(cliente.id);
    $('#db_tel_cliente').html(cliente.telefono);
    $('#db_dir_cliente').html(cliente.direccion);
    $('#db_deuda_cliente').html("$ " + cliente.adeudado);
    $('#db_nombre_cliente').html(cliente.nombre_completo);


  }
});