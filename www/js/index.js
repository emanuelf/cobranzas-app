var myApp = new Framework7();

$(document).ready(function() {
  var endPoint = window.localStorage.getItem('end_point');
  var user = JSON.parse(window.localStorage.getItem('user'));
  $.get(endPoint + 'services/my_services' + '?cobrador_id=' + user.debt_collector_id , function(data) {
    var ulServicios = $('#lista-servicios');
    ulServicios.empty();
    window.localStorage.setItem('servicios', JSON.stringify(data));
    $.each(data, function(index, service) {
      var template = $('#template-li-servicio').html();
      template = template.replace('{{cliente}}', service.customer.nombre_completo)
                         .replace('{{fecha_servicio}}', service.fecha_to_s)
                         .replace('{{descripcion}}', service.descripcion)
                         .replace('{{monto}}', service.monto)
                         .replace('{{adeudado}}', service.adeudado)
                         .replace('{{pagado}}', service.pagado)
                         .replace('{{nro_servicio}}', service.id)
                         .replace('{{tipo_servicio}}', service.service_type.nombre)
      ulServicios.append(template);
    });
  });

  window.prepararPago = function() {
    var trCuota = $('tr[data-cuota-a-pagar="true"]');
    if (trCuota.length==0) {
      alert('Nada para pagar');
      return;
    } else {
      myApp.pickerModal('.picker-1');
    }
    $('#pc-nro-servicio').val($('#db-nro-servicio').html());
    $('#pc-cuota-nro').val($(trCuota).find('.nro').html());
    $('#pc-monto').val($(trCuota).find('.saldo').html());
    $('#pc-cobrador').val(user.debt_collector.apynom);
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    $('#pc-fecha').val(today);
  }

  $(document).on('click', '#enviar-pago-cuota', function(event) {
    event.preventDefault();
    var idCuota     = $('tr[data-cuota-a-pagar="true"]').data('id-cuota');
    var monto       = $('#pc-monto').val();
    var comentarios = $('#pc-comentarios').val();
    var fecha       = $('#pc-fecha').val();
    var params = {
      'fee_payment[fee_id]': idCuota,
      'fee_payment[monto]':  monto,
      'fee_payment[comentarios]': comentarios,
      'fee_payment[fecha_pago]': fecha,
      'fee_payment[debt_collector_id]': user.debt_collector_id
    }
    $.post(endPoint + 'services/fee_payment', params)
     .done(function(data) {
        alert('¡Pago registrado!');
        location.reload();
     })
     .fail(function(error){
        alert('Errores al guardar el pago');
     });
  });

  window.mostrarServicio = function (idServicio) {
    var servicios = JSON.parse(window.localStorage.getItem('servicios'));
    var servicio;
    $.each(servicios, function(index, s) {
      if (s.id == idServicio) {
        servicio = s;
        return false;
      }
    });
    if (servicio.puede_pagar) {
      $('#boton-pagar-cuota').show();
    } else {
      $('#boton-pagar-cuota').hide();
    }

    $('h3.nombre-cliente').html(servicio.customer.nombre_completo + ' (' + servicio.service_type.nombre+')');
    $('#db-monto-servicio').html('$ '  + servicio.monto);
    $('#db-deuda-servicio').html('$ '  + servicio.adeudado);
    $('#db-pagado-servicio').html('$ '  + servicio.pagado);

    $('#db-nro-servicio').html(servicio.id);

    $('#db-fecha-servicio').html(servicio.fecha_to_s);
    $('#db-periodo-pago-servicio').html(servicio.periodo_pago);
    $('#db-descripcion-servicio').html(servicio.descripcion);
    $('#db-comentarios-servicio').html(servicio.comentarios);

    var tbodyCuotas = $('#cuotas-servicio');
    tbodyCuotas.empty();
    $.each(servicio.fees_data, function(index, fee_data) {
      tbodyCuotas.append(
        '<tr class="cuota" data-id-cuota="'+fee_data.id+'" data-cuota-a-pagar="'+fee_data.cuota_a_pagar+'">'+
          '<td class="nro">'+fee_data.cuota_nro+'</td>'+
          '<td class="monto-establecido">'+fee_data.monto_establecido+'</td>'+
          '<td class="monto-variable"> '+fee_data.monto_variable+'</td>'+
          '<td class="vencimiento">'+fee_data.vencimiento+'</td>'+
          '<td class="pagado"> '+fee_data.pagado+'</td>'+
          '<td class="saldo" data-saldo="'+fee_data.saldo+'"> '+fee_data.saldo+'</td>'+
        '</tr>'
      );
    });
  }
});