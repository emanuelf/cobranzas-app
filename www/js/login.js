//window.localStorage.setItem('end_point', 'http://192.168.0.103:3000/api/');
//window.localStorage.setItem('end_point', 'http://localhost:3000/api/');

window.localStorage.setItem('end_point', 'http://cobranzas-salon.herokuapp.com/api/');
var endPoint = window.localStorage.getItem('end_point');

$(document).ready(function() {
  $('#login').click(function(event) {
    var params  = {
      username: $('#username').val(),
      password: $('#password').val()
    }
    $.post(endPoint + 'users/login', params)
     .done(function(user) {
       window.localStorage.setItem('user', JSON.stringify(user));
       location.href = "index.html"
     })
     .fail(function (error) {
       alert('Usuario o contraseña incorrectos');
     });
  });
});