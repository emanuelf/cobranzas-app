$(function() {
  var endPoint = window.localStorage.getItem('end_point');
  var user = JSON.parse(window.localStorage.getItem('user'));

  $.get(endPoint + 'customers/atrasos?cobrador_id=' + user.debt_collector_id, function(data) {
    var ulAtrasados = $('#lista-atrasados');
    ulAtrasados.empty();
    $.each(data, function(index, customer) {
      var template = $('#template-li-atrasados').html();
      template = template.replace('{{cliente}}', customer.nombre_completo)
                         .replace('{{adeudado}}', customer.adeudado)
                         .replace('{{cuotas_atrasadas}}', customer.cantidad_cuotas_vencidas);
      ulAtrasados.append(template);
    });
  });
});